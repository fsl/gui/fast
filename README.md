# FAST GUI

This is the `fast_gui`. It is developed as a React web app, and is is intended to be loaded in the [`fsl-desktop`](https://git.fmrib.ox.ac.uk/fsl/gui/desktop) electron app.

# Development

## Prerequisites

It is assumed that your development machine is either a Mac or Linux machine.

- [nodejs and NPM](https://nodejs.org/en/)

## Setup

Clone and cd into this repo

```bash
git clone https://git.fmrib.ox.ac.uk/fsl/gui/fast.git

cd fast
```

Install dependencies via npm

```bash
npm install
```

## Running the GUI during development

**Important**: this GUI will be served on port `5174` and this is hard-coded in `vite.config.js`. This is the port that the `fsl-desktop` app expects the GUI to be served on during development. If you change this port, you will need to update the `fsl-desktop` app to match.

```bash
npm run dev
```

Then, in a separate terminal, start the `fsl-desktop` app

```bash
cd <path/to/fsl-gui-desktop/repo>
npm run dev
```

You will be able to see changes in real-time in the `fsl-desktop` app while you develop. 

## Building the GUI for production

This is not necessary during normal development. This step is performed automatically when a new tag is pushed to the repo. 

New tags should be of the form `X.Y.Z`, where `X.Y.Z` is the SemVer version number of the GUI and should match the version number listed in `package.json`.

New tags pushed to the repo will trigger a new build and release in the _recipe_ repo [here](https://git.fmrib.ox.ac.uk/fsl/conda/fsl-gui-fast). The release button must be pressed manually to trigger the release.

```bash
npm run build
```


