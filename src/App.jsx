import { useState, useEffect } from 'react'
import './App.css'
import {fsljs} from '@fmrib/fsljs'
import { 
  AppContainer, 
  Title, 
  FileChooser, 
  Row, 
  Column, 
  CommandPreview, 
  InputSlider,
  InputSelect,
  InputNumber
} from '@fmrib/widgets' // '../../widgets/lib/widgets'
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import CssBaseline from '@mui/material/CssBaseline';

export default function App() {

  const [optionsOpen, setOptionsOpen] = useState(false)
  const [inputs, setInputs] = useState([''])
  const [opts, setOpts] = useState(fsljs.defaultFastOpts)
  const [commandString, setCommandString] = useState('')
  const [running, setRunning] = useState(false)

  // watch for changes to the JSON string version of opts
  useEffect(() => {
    // create the command string for this tool (sends a message to the main process)
    async function createCommandString(){
      let command = 'fast'
      let commandString = await fsljs.createCommandString({command, opts})
      setCommandString(commandString)
    }
    autoSetOutputFile()
    createCommandString()
  }, [JSON.stringify(opts)])

  // watch for length of inputs and update -S
  useEffect(() => {
    autoSetOutputFile()
    // join the inputs into a string and set opts['in']
    let inputsString = inputs.join(' ')
    console.log(inputsString)
    let imType = {}
    if (inputs.length === 1){
      imType = {'-t': opts['-t']}
    }
    setOpts({
      ...opts, 
      'in': inputsString, 
      '-S': inputs.length,
      ...imType
    })
  }, [JSON.stringify(inputs)])

  // useEffect(() => {
  //   autoSetOutputFile()
  //   let inputsString = inputs.join(' ')
  //   console.log(inputsString)
  //   setOpts({...opts, 'in': inputsString})
  // }, [JSON.stringify(inputs)])

  function onOptionsClick(){
    setOptionsOpen(!optionsOpen)
  }

  function onImageTypeChange(value){
    setOpts({...opts, '-t': value})
  }

  function onChannelsChange(value){
    // get current inputs
    let currentInputs = [...inputs]
    // if the value is greater than the current number of inputs, add an input
    if (value > currentInputs.length){
      // get the difference
      let difference = value - currentInputs.length
      // add the difference to the current inputs
      for (let i = 0; i < difference; i++){
        currentInputs.push('')
      }
    }
    // if the value is less than the current number of inputs, remove an input
    if (value < currentInputs.length && currentInputs.length > 1){
      // get the difference
      let difference = currentInputs.length - value
      for (let i = 0; i < difference; i++){
        currentInputs.pop()
      }
    }
    // update the inputs
    setInputs(currentInputs)
  }

  function onNumTissueClassesChange(value){
    setOpts({...opts, '-n': value})
  }

  const imageTypeOptions = [
    {value: 1, label: 'T1'},
    {value: 2, label: 'T2'},
    {value: 3, label: 'Proton Density'}
  ]

  async function onInputFileClick(i){
    let results = await fsljs.openFileDialog()
    console.log(results)
    let inputFile = results.filePaths[0]
    // update the inputs
    let newInputs = [...inputs]
    newInputs[i] = inputFile
    setInputs(newInputs)
    // setOpts({...opts, 'in': inputFile})
  }

  function autoSetOutputFile(){
    // let inputFile = opts['in']
    let inputFile = inputs[0]
    // do nothing if the input file is empty
    if (inputFile === ''|| inputFile === null) return
    let inputDirname = fsljs.dirname(inputFile)
    let inputBasename = fsljs.removeExtension(fsljs.basename(inputFile))
    let outputFile = fsljs.join(inputDirname, `${inputBasename}`)
    setOpts({...opts, '-o': outputFile})
  }

  async function onRunClick(){
    setRunning(true)
    let output = await fsljs.run({
      command: 'fast',
      opts: opts
    })
    console.log('from main', output)
    setRunning(false)
  }


  return (
    <AppContainer gap={2} margin={2}>
      <CssBaseline />
      {/* title */}
      <Title>
        FAST
      </Title>
      {/* number of input channels (images) */}

      {/* <InputSlider
        label="Number of input channels (images)"
        value={1}
        max={10}
        min={1}
        step={1}
        onChange={onChannelsChange}
      /> */}

      <InputNumber
        textLabel="Number of input channels (images)"
        value={opts['-S']}
        max={10}
        min={1}
        step={1}
        onChange={onChannelsChange}
      />
      
      {/* <FileChooser 
        value={opts['in']} 
        textLabel={`Input image`}
        onClick={onInputFileClick}
      /> */}

      {/* input image chooser loop over inputs */}
      {inputs.map((input, i) => {
        return (
          <FileChooser
            key={i}
            value={input}
            textLabel={`Input image ${i+1}`}
            onClick={() => onInputFileClick(i)}
          />
        )
      })}

      {/* image type select */}
      {inputs.length === 1 && 
        <Row width={'100%'}>
          <InputSelect options={imageTypeOptions} value={opts['-t']} label="Image type" width={'50%'} onChange={onImageTypeChange}/>
        </Row>
      }
      {/* output image basename */}
      <FileChooser value={opts['-o']} textLabel="Output image basename"/>
      {/* number of tissue classes to segment */}
      <InputSlider
        label="Number of tissue classes to segment"
        value={3}
        max={6}
        min={2}
        step={1}
        onChange={onNumTissueClassesChange}
      />
      <CommandPreview commandString={commandString} multiLine={true}/>
      {/* Row of buttons (run and options) */}
      <Row gap={2} marginTop={2}>
        {/* Run button */}
        <Button variant='contained' onClick={onRunClick} disabled={running}>Run</Button>
        {/* Options button */}
        <Button onClick={onOptionsClick}>Options</Button>
      </Row>
        {optionsOpen &&
          <Column gap={2} justifyContent={'left'} width={'100%'}>
            <Row>
              {/* binary segmentation one image per class */}
              <FormControlLabel
              control={
                <Checkbox
                  checked={opts['-g']}
                  onChange={(e) => setOpts({...opts, '-g': e.target.checked})}
                />
              }
              label="Binary segmentation (output one image per class)"
            />
            </Row>
            <Row>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={!opts['--nopve']}
                    onChange={(e) => setOpts({...opts, '--nopve': !e.target.checked})}
                  />
                }
                label="Partial volume maps"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={opts['-B']}
                    onChange={(e) => setOpts({...opts, '-B': e.target.checked})}
                  />
                }
                label="Restored input"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={opts['-b']}
                    onChange={(e) => setOpts({...opts, '-b': e.target.checked})}
                  />
                }
                label="Estimated bias field"
              />
            </Row>
            {/* main MRF parameter */}
            <InputSlider
              label="Main MRF parameter"
              value={opts['-H']}
              max={1}
              min={0}
              step={0.01}
              onChange={(value) => setOpts({...opts, '-H': value})}
            />
            {/* number of iterations for bias field removal */}
            <InputSlider
              label="Number of iterations for bias field removal"
              value={opts['-I']}
              max={20}
              min={1}
              step={1}
              onChange={(value) => setOpts({...opts, '-I': value})}
            />
            {/* bias field smoothing extent MM */}
            <InputSlider
              label="Bias field smoothing FWHM (mm)"
              value={opts['-l']}
              max={100} // is this right? whould we want more than 100mm?
              min={0}
              step={1}
              onChange={(value) => setOpts({...opts, '-l': value})}
            />
            <Row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={false}
                  onChange={(e) => {}}
                />
              }
              label="Use a-proiri probability maps for initialisation"
            />
            </Row>
            <Row>
            <FormControlLabel
              control={
                <Checkbox
                  checked={false}
                  onChange={(e) => {}}
                />
              }
              label="Use a-proiri probability maps for final segmentation"
            />
            </Row>
            {/* input chooser for standard to input FLIRT transform */}
            <FileChooser value={''} textLabel="Standard to input FLIRT transform"/>
            {/* input chooser for initial tissue-type means */}
            <FileChooser value={''} textLabel="Initial tissue-type means"/>


          </Column>
        }

    </AppContainer>
  )
}
